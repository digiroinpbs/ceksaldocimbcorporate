package id.corechain

import com.rethinkdb.RethinkDB
import com.rethinkdb.gen.exc.ReqlDriverError
import com.rethinkdb.gen.exc.ReqlError
import com.rethinkdb.gen.exc.ReqlPermissionError
import com.rethinkdb.net.Connection
import org.apache.commons.codec.digest.DigestUtils
import org.openqa.selenium.*
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.support.ui.Select
import java.util.*
import redis.clients.jedis.Jedis
import redis.clients.jedis.exceptions.JedisConnectionException
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap
import java.awt.SystemColor.window
import java.io.*
import java.time.MonthDay
import java.time.Year
import com.sun.deploy.ui.CacheUpdateProgressDialog.dismiss




class WebDriver(){
    fun getWebDriver(): WebDriver? {
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        var driver: WebDriver
        System.setProperty("webdriver.chrome.driver", props.get("webdriver.chrome.driver").toString())
        driver = ChromeDriver()
        return driver
    }
    fun getDBName(): String? {
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        return props.get("db.name").toString()
    }
    fun getRedisParam(): HashMap<String,String>? {
        val map:HashMap<String,String> = HashMap<String,String>()
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        map.put("ip",props.get("redis.name").toString())
        map.put("port",props.get("redis.port").toString())
        map.put("password",props.get("redis.password").toString())
        map.put("db",props.get("redis.db").toString())
        return map
    }
    fun directory(): String? {
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        return props.get("directory.download").toString()
    }
}

fun main(args: Array<String>) {
    var result:String
    try {
        result= checkSaldo("ID28503KBW","adryan1","Corechain123")
    }catch (e:ReqlDriverError){
        System.out.println("connection rethink DB error")
    }catch (e:JedisConnectionException){
        System.out.println("connection redis error")
        e.printStackTrace()
    }catch (e:ReqlPermissionError){
        System.out.println("connection rethink DB permission Error")
    }catch (e: ReqlError){
        System.out.println("rethink DB problem")
    }catch (e:Exception){
        e.printStackTrace()
        id.corechain.WebDriver().getWebDriver()!!.quit()
    }

    return
}

fun checkSaldo(corpId:String, username:String, password:String):String{
    var redisParam:HashMap<String,String> = id.corechain.WebDriver().getRedisParam() as HashMap<String, String>
    val jedis = Jedis(redisParam.get("ip"), redisParam.get("port")!!.toInt())
    if(!redisParam.get("password").equals("")){
        jedis.auth(redisParam.get("password"))
    }
    jedis.select(redisParam.get("db")!!.toInt())
    var result =""
    val webDriver =id.corechain.WebDriver().getWebDriver()
    if(webDriver!=null){
        var error =false
        var alertError =false
        try{
            webDriver.get("https://bizchannel.cimbniaga.co.id/corp/common2/login.do?action=loginRequest")
            var element = webDriver.findElement(By.name("corpId"))
            element.sendKeys(corpId)
            element = webDriver.findElement(By.name("userName"))
            element.sendKeys(username)
            element = webDriver.findElement(By.name("passwordEncryption"))
            element.sendKeys(password)
            webDriver.findElement(By.name("submit1")).click()

            webDriver.switchTo().defaultContent()
            var frame = webDriver.findElement(By.name("menuFrame"))
            webDriver.switchTo().frame(frame)
            webDriver.findElement(By.xpath("//div[@onclick='SwitchMenu(\"sub\" + 1)']")).click()
            webDriver.findElement(By.xpath("//span[@id='sub1']//a[@id='subs9']")).click()

            jedis.set("CIMB:lastLogin",Date().time.toString())

            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("mainFrame"))
            webDriver.switchTo().frame(frame)
            var balance =webDriver.findElement(By.xpath("//table[@cellspacing='1']//tbody//tr[@class='clsEven']//td[@align='right']//a")).text
            balance = balance.replace(".00","")
            balance = balance.replace(",",".")
            var now =System.currentTimeMillis() / 1000L
            result=result+"{\"bank\":\"CIMB\",\"amount\":\""+balance+"\",\"lastupdate\":\""+now+"\"}"
            jedis.set(username+":CIMB:"+now,result)
            jedis.set(username+":CIMB:lastest",result)

            result ="result :["
            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("menuFrame"))
            webDriver.switchTo().frame(frame)
            webDriver.findElement(By.xpath("//span[@id='sub1']//a[@id='subs10']")).click()

            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("mainFrame"))
            webDriver.switchTo().frame(frame)
            webDriver.findElement(By.xpath("//a//img[@src='/common/image/picklist.gif']")).click()


            val parentWindowHandler = webDriver.getWindowHandle()
            var subWindowHandler: String? = null

            val handles = webDriver.getWindowHandles()
            val iterator = handles.iterator()
            while (iterator.hasNext()) {
                subWindowHandler = iterator.next()
            }
            webDriver.switchTo().window(subWindowHandler)
            webDriver.findElement(By.xpath("//a")).click()
            webDriver.switchTo().window(parentWindowHandler)
            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("mainFrame"))
            webDriver.switchTo().frame(frame)

            /*webDriver.findElement(By.xpath("//input[@value='second']")).click()*/
            var drop:Select = Select(webDriver.findElement(By.xpath("//select[@name='customFile']")))
            drop.selectByValue("CSV")
            webDriver.findElement(By.xpath("//input[@name='download1']")).click()

            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("menuFrame"))
            webDriver.switchTo().frame(frame)
            webDriver.findElement(By.xpath("//span[@id='sub1']//a[@id='subs11']")).click()
            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("mainFrame"))
            webDriver.switchTo().frame(frame)
            webDriver.findElement(By.xpath("//table[@class='clsForm']//tbody//tr//td//a[1]")).click()
            webDriver.findElement(By.xpath("//table[@class='clsForm']//tbody//tr//td//input[@name='chkParent']")).click()
            webDriver.findElement(By.xpath("//input[@name='deleteButton']")).click()
            if (webDriver.switchTo().alert() != null) {
                val alert = webDriver.switchTo().alert()
                alert.accept()

            }


            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("topFrame"))
            webDriver.switchTo().frame(frame)
            webDriver.findElement(By.xpath("//a[@href='/corp/common2/login.do?action=logout']")).click()
            TimeUnit.SECONDS.sleep(5)
            var directory = id.corechain.WebDriver().directory()
            val folder = File(directory + "/")
            val listOfFiles = folder.listFiles()
            for (i in listOfFiles.indices) {
                if (listOfFiles[i].isFile()) {
                    if (listOfFiles[i].getName().contains(".csv")) {
                        var nameFile:String =""+Date().time+listOfFiles[i].getName()
                        listOfFiles[i].renameTo(File(directory + "/mutasi/" +  nameFile+ ""))
                        listOfFiles[i].delete()
                        val csvFile = directory + "/mutasi/" + nameFile + ""
                        val cvsSplitBy = ","
                        var counter = 0
                        try {
                            BufferedReader(FileReader(csvFile)).use { br ->
                                var indicator = false
                                var test = true;
                                while (test) {
                                    counter++
                                    val line = br.readLine()
                                    if(line==null){
                                        break
                                    }
                                    if (counter == 5 || indicator == true) {
                                        var splittedLine = line.split(",")
                                        if (splittedLine[0].equals("No Record Found")) {
                                            break
                                        } else {
                                            try {
                                                var res = ""
                                                var debitIndicator =false
                                                indicator = true
                                                var splitDate = splittedLine[0].split(" ")
                                                splitDate = splitDate[0].split("/")
                                                var date=""
                                                date = ""+ Year.now()+splitDate[0]+splitDate[1]
                                                var type = ""
                                                var amount =0
                                                if(splittedLine[4].equals("")){
                                                    amount = splittedLine[5].replace(".00", "").toInt()
                                                }else{
                                                    debitIndicator =true
                                                    amount = splittedLine[4].replace(".00", "").toInt()
                                                }
                                                res = res + "{\"Tanggal\":\"" + date + "\","
                                                res = res + "\"Keterangan Transaksi\":\"" + splittedLine[3] + "\","

                                                if (!debitIndicator) {
                                                    type = "in"
                                                    res = res + "\"Debet\":\"" + 0 + "\","
                                                    res = res + "\"Kredit\":\"" + amount + "\"}"
                                                } else {
                                                    type = "out"
                                                    res = res + "\"Debet\":\"" + amount + "\","
                                                    res = res + "\"Kredit\":\"" + 0 + "\"}"
                                                }
                                                val r: RethinkDB = RethinkDB.r;
                                                val conn: Connection = r.connection().hostname("rth0.corechain.id").port(28015).db(id.corechain.WebDriver().getDBName()).user("digiro", "digiro").connect()
                                                r.table("mutasi").insert(r.hashMap("id", sha1converter(res)).with("date", date.toInt()).with("desc", splittedLine[1]).with("type", type).with("amount", amount).with("processed", -1).with("bank", "BCA")
                                                ).run<String>(conn)
                                                conn.close()
                                            } catch (e: IndexOutOfBoundsException) {
                                                e.printStackTrace()
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        break
                    }
                }
            }
        }catch (e:NullPointerException){
            print("Directory not found")
            error=true
        }catch (e: java.util.NoSuchElementException){
            print("Problem")
            error=true
        }catch (e:ReqlDriverError){
            System.out.println("connection rethink DB error")
            error=true
        }catch (e:JedisConnectionException){
            e.printStackTrace()
            System.out.println("connection redis error")
            error=true
        }catch (e:ReqlPermissionError){
            System.out.println("connection rethink DB permission Error")
            error=true
        }catch (e:UnhandledAlertException){
            print("alert exception")
            alertError=true
            error=true
        }catch (e: ReqlError){
            System.out.println("rethink DB problem")
            error=true
        }catch (e:Exception){
            e.printStackTrace()
            error=true
        }finally {
            if(alertError){
                val alert = webDriver.switchTo().alert()
                alert.dismiss()
            }
            if(error){
                webDriver.switchTo().defaultContent()
                var frame = webDriver.findElement(By.name("topFrame"))
                webDriver.switchTo().frame(frame)
                webDriver.findElement(By.xpath("//a[@href='/corp/common2/login.do?action=logout']")).click()
                TimeUnit.SECONDS.sleep(5)
            }
            jedis.quit()
            webDriver.quit()
        }
    }
    return result;
}

fun sha1converter(param: String): String {
    return DigestUtils.sha1Hex(param)
}
